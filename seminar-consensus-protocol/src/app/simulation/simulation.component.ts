import {Component, ComponentFactoryResolver, Input, OnInit, Type, ViewChild} from '@angular/core';
import {RenderDirective} from "../render.directive";
import {ComponentFactory} from "@angular/core/src/linker/component_factory";
import {SimulationService} from "../model/simulation.service";
import {Protocol} from "../model/protocol";
import {Link} from "../model/link";
import {ColorState} from "../model/color-state";
import {MatIconRegistry} from "@angular/material";
import {ProtocolServiceService} from "../protocol-service.service";
import {Node} from "../model/node";
import {DomSanitizer} from "@angular/platform-browser";
import {Snowball} from "../model/snowball";

//todo remove. use Error instead
import * as assert from "assert";
import {SimulationPack} from "../model/simulation-pack";


enum Topology {P2P, STAR, GRID};


@Component({
  selector: 'app-simulation',
  templateUrl: './simulation.component.html',
  styleUrls: ['./simulation.component.css']
})
export class SimulationComponent implements OnInit {
  @Input() simulationPack: SimulationPack[];
  @ViewChild(RenderDirective) render: RenderDirective;

  private defaultProtocol: Protocol = null;

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private simulationService: SimulationService,
              iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon(
      'play-button',
      sanitizer.bypassSecurityTrustResourceUrl('assets/simulation/progress-bar/play-button.svg'))
      .addSvgIcon('step-forward-button',
        sanitizer.bypassSecurityTrustResourceUrl('assets/simulation/progress-bar/step-forward.svg'))
      .addSvgIcon('pause-button',
        sanitizer.bypassSecurityTrustResourceUrl('assets/simulation/progress-bar/pause-button.svg'))
      .addSvgIcon('reset-button',
        sanitizer.bypassSecurityTrustResourceUrl('assets/simulation/progress-bar/reset-button.svg'));
  }

  ngOnInit() {//TODO make simulation pack adjustable
    console.log(this.simulationPack);
    let firstSimulationPack: SimulationPack = this.simulationPack[0];
    let firstRendererType: Type<any> = firstSimulationPack.getRenderer();

    let componentFactory = this.componentFactoryResolver.resolveComponentFactory(firstRendererType);
    this.showComponent(componentFactory);

    this.defaultProtocol = firstSimulationPack.getProtocol();

    this.updateModel();
  }

  showComponent(componentFactory: ComponentFactory<any>) {
    let viewContainerRef = this.render.viewContainerRef;
    viewContainerRef.clear();

    let componentRef = viewContainerRef.createComponent(componentFactory);
  }




  /* Parameter */
  nodeCount: number = 8;
  topology: Topology = Topology.P2P


  /* Model */
  nodes: Node[] = [];
  links: Link[] = [];



  updateModel() {
    //todo export to a snowball controller
    console.log('initial simulation parameters are updated');

    this.setNodeCount(this.nodeCount);
    this.setEdges(this.topology);
    this.simulationService.setNodes(this.nodes);
    this.simulationService.setLinks(this.links);

    if(this.nodes.length >0) {
      let firstNode = this.nodes[0];
      this.simulationService.setInitialNode(firstNode);
    }
  }



  // todo only experimental, in future a functional set description instead of a real set should be used
  private setEdges(t: Topology): void {
    this.links = [];
    switch (t) {
      case Topology.P2P:
        for (let node1 of this.nodes) {
          for(let node2 of this.nodes) {
            if(node1 !== node2) {
              let link: Link = new Link(0, node1, node2);
              this.links.push(link);
              node1.addLink(link);
              node2.addLink(link);
            }
          }
        }
        break;
      default:
        throw new Error('Topologies other than P2P are not yet implemented');
    }
  }

  private setNodeCount(c: number): void {
    assert(c > 0); //check if positive
    assert(c % 1 === 0); //check if whole number

    let oldNodeCount: number = this.nodes.length;
    if (oldNodeCount < c) { //increase node count
      for (let i = oldNodeCount; i < c; i++) {
        this.nodes.push(new Node(this.defaultProtocol, i));
      }
    } else if (oldNodeCount > c) { //reduce node count
      this.nodes = this.nodes.slice(0, c);
    }

    assert.equal(this.nodes.length, c);
  }




  simulationStepForward(): void {
    console.log('simulation step forward');
    this.simulationService.simulation$.doStep();
  }


}
