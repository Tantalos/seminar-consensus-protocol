import {Component, OnInit} from '@angular/core';
import {Link} from "../model/link";
import {ColorState} from "../model/color-state";
import {Node} from "../model/node";
import {getColorShades} from "../utils/color-utils";
import {RendererComponent} from "../renderer/renderer.component";

import * as d3 from 'd3';
import * as Color from 'color';
import {simLog} from "../log-config";
import {SnowballByzantine} from "../model/snowball-byzantine";
import {Protocol} from "../model/protocol";
import {Snowball} from "../model/snowball";

@Component({
  selector: 'app-snowball-graph',
  templateUrl: './snowball-graph.component.html',
  styleUrls: ['./snowball-graph.component.css']
})
export class SnowballGraphComponent extends RendererComponent implements OnInit {


  onSimulationChange(): void {
    super.onSimulationChange();
    this.draw();
  }

  draw() {
    console.log('run snowball draw');

    /* Model */
    //todo check if model has to be joined again.
    let nodes: Node[] = this.simulation.simulation$.network.nodes;
    let links: Link[] = this.simulation.simulation$.network.links;

    //todo this is only a workaround, because forces link need source & target attributes
    let rewriteSourceTarget = function (l: Link) {
      l['source'] = l.node1;
      l['target'] = l.node2
    };
    links.forEach(rewriteSourceTarget);


    this.drawNodes(nodes);
    this.drawLinks(nodes);
  }

  private drawNodes(nodes: Node[]) {
    let n = nodes.length;

    // bind nodes to circles
    this.svg.selectAll('circle').data(nodes).enter().append('circle');
    // this.svg.selectAll('line').data(links).enter().append('line');
    this.svg.selectAll('circle').attr('r', 30)

      // fill node with color shade depending of confidence counter
      .attr('fill', confidenceOfNode.bind(this.simulation))



      // frame node with color of state
      .attr('stroke', currentColorOfNode)
      .attr('stroke-width', 2)


      // enable changing initial node and color by double clicking
      .on('click', changeInitial.bind(this.simulation))

      // changes the protocol of a node to byzantine or snowball
      .on('contextmenu', flipByzantine.bind(this.simulation))
      // arrange node in a circle according to their index
      .attr('cx', (d, i) => {
        return this.x_scale(Math.sin(this.index_to_rad(i, n)));
      })
      .attr('cy', (d, i) => {
        return this.y_scale(Math.cos(this.index_to_rad(i, n)));
      });


    // overwrite filling of byzantine nodes
    this.svg.selectAll('circle').data(nodes)
      .filter((d: Node) => {return d.protocol.name === 'snowball-byzantine';})
      .attr('fill', 'url(#byzantine)');
  }


  private drawLinks(nodes: Node[]) {
    let n = nodes.length;

    this.svg.selectAll('line')
      .attr('stroke-width', 2).attr('stroke', 'black')
      .attr('x1', (d) => {
        return this.x_scale(Math.sin(this.index_to_rad(d.source.nodeID, n)));
      })
      .attr('x2', (d) => {
        return this.x_scale(Math.sin(this.index_to_rad(d.target.nodeID, n)));
      })
      .attr('y1', (d) => {
        return this.y_scale(Math.cos(this.index_to_rad(d.source.nodeID, n)));
      })
      .attr('y2', (d) => {
        return this.y_scale(Math.cos(this.index_to_rad(d.target.nodeID, n)));
      });
  }

  private x_scale;
  private y_scale;

  /* Parameter: initialize after DOM is created */
  private svg;
  private width;
  private height;

  private margin;
  private x_center;
  private y_center;
  private radius;


  ngOnInit() {
    super.ngOnInit();

    /* Parameter */
    this.svg = d3.select("svg");
    this.width = this.svg.attr("width");
    this.height = this.svg.attr("height");

    this.margin = 100;
    this.x_center = this.width / 2;
    this.y_center = this.height / 2;
    this.radius = (this.height - 2 * this.margin) / 2;

    this.x_scale = d3.scaleLinear()
      .domain([0, 1])
      .range([this.x_center, this.x_center + this.radius]);

    this.y_scale = d3.scaleLinear()
      .domain([0, 1])
      .range([this.y_center, this.y_center + this.radius]);

  }


  private index_to_rad(index, n_elements): number {
    return 2 * Math.PI * index / n_elements;
  }

}


const ACCEPTED_RED: Color = Color({r: 255, g: 0, b: 0});
const UNCERTAIN_RED: Color = Color({r: 160, g: 140, b: 140});
const ACCEPTED_BLUE: Color = Color({r: 0, g: 0, b: 255});
const UNCERTAIN_BLUE: Color = Color({r: 140, g: 140, b: 160});
const UNKNOWN_GRAY: Color = Color({r: 140, g: 140, b: 140});

function confidenceOfNode(d: Node) {
  switch (d['col']) {
    case ColorState.Red:
      return getColorShades(this.simulation$.maxConfidenceCounter, d['d'][ColorState.Red], UNCERTAIN_RED, ACCEPTED_RED).string();
      break;
    case ColorState.Blue:
      return getColorShades(this.simulation$.maxConfidenceCounter, d['d'][ColorState.Blue], UNCERTAIN_BLUE, ACCEPTED_BLUE).string();
      break;
    default:
      return UNKNOWN_GRAY;
  }
}

function currentColorOfNode(d: Node) {
  switch (d['col']) {
    case ColorState.Red:
      return ACCEPTED_RED;
      break;
    case ColorState.Blue:
      return ACCEPTED_BLUE;
      break;
    default:
      return UNKNOWN_GRAY;
  }
}

function changeInitial(d: Node) {
  if (d === this.simulation$.initial) {
    let newArgs = flipColor(this.args);
    this.setProtocolSpecificArgs(newArgs);
  } else {
    simLog.info('Set Node ' + d.nodeID + ' to initial node');
    this.setInitialNode(d);
  }
}

function flipColor(args) {
  let newArgs = args;
  for (let i in args) {
    let entry = args[i];
    if (entry.initialColor !== undefined) {
      let color: ColorState = entry.initialColor;
      if (color === ColorState.Red) {
        simLog.info('Flip color of initial node from ' + ColorState[ColorState.Red] + 'to' + ColorState[ColorState.Blue] );
        entry.initialColor = ColorState.Blue;
      } else if (color === ColorState.Blue) {
        simLog.info('Flip color of initial node from ' + ColorState[ColorState.Blue] + 'to' + ColorState[ColorState.Red] );
        entry.initialColor= ColorState.Red;
      }

      newArgs[i] = entry;
      break;
    }
  }
  return newArgs;
}


const byzantineProtocol: Protocol = new SnowballByzantine();
const snowballProtocol: Protocol = new Snowball();
function flipByzantine(d: Node) {
  if(d.protocol.name === snowballProtocol.name) {
    simLog.info('Change protocol of Node ' + d.nodeID +' to ' + byzantineProtocol.name);
    this.setProtocol(d.nodeID, byzantineProtocol);
  } else if(d.protocol.name === byzantineProtocol.name) {
    simLog.info('Change protocol of Node ' + d.nodeID +' to '+snowballProtocol.name);
    this.setProtocol(d.nodeID, snowballProtocol);
  } else {
    throw new Error('Illegal state');
  }
  d3.event.preventDefault();
}

function ifDefined(attrName: string): (d:any) => boolean {
  return (d) => {return d[attrName] != null};
}


