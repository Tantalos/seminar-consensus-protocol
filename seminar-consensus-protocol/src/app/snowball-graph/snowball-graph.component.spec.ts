import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SnowballGraphComponent } from './snowball-graph.component';

describe('SnowballGraphComponent', () => {
  let component: SnowballGraphComponent;
  let fixture: ComponentFixture<SnowballGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SnowballGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SnowballGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
