import {Category,CategoryLogger,CategoryServiceFactory,CategoryConfiguration,LogLevel} from "typescript-logging";

// Optionally change default settings, in this example set default logging to Info.
// Without changing configuration, categories will rootlog to Error.
CategoryServiceFactory.setDefaultConfiguration(new CategoryConfiguration(LogLevel.Info));

// Create categories, they will autoregister themselves, one category without parent (root) and a child category.
export const logConfig = new Category("consensus");
export const simLog = new Category("simulation-service", logConfig);

export const protocolLog = new Category("protocol", logConfig);
export const snowballLog = new Category("snowball-protocol", protocolLog);



// Optionally get a logger for a category, since 0.5.0 this is not necessary anymore, you can use the category itself to rootlog.
// export const rootlog: CategoryLogger = CategoryServiceFactory.getLogger(cat);