import { Component, OnInit } from '@angular/core';
import {ColorState} from "../model/color-state";
import {SimulationService} from "../model/simulation.service";

@Component({
  selector: 'app-snowball-controller',
  templateUrl: './snowball-controller.component.html',
  styleUrls: ['./snowball-controller.component.css']
})
export class SnowballControllerComponent implements OnInit {

  initialColor: ColorState = ColorState.Blue;
  maxConfidenceCounter = 6;
  sampleSize = 5;
  alpha = 0.6;

  constructor(private simulationService: SimulationService) {}

  ngOnInit() {
    this.updateModel();
  }

  updateModel() {
    let args = [
      {initialColor: this.initialColor},
      {maxConfidenceCounter: this.maxConfidenceCounter},
      {sampleSize: this.sampleSize},
      {alpha: this.alpha}
    ];
    this.simulationService.setProtocolSpecificArgs(args);
  }


}
