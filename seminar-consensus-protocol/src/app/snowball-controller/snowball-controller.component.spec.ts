import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SnowballControllerComponent } from './snowball-controller.component';

describe('SnowballControllerComponent', () => {
  let component: SnowballControllerComponent;
  let fixture: ComponentFixture<SnowballControllerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SnowballControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SnowballControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
