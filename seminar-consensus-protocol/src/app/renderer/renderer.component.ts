import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit
} from '@angular/core';
import {Renderer} from "../model/renderer";
import {SimulationService} from "../model/simulation.service";
import {SimulationObserver} from "../model/simulation-observer";

@Component({
  selector: 'app-renderer',
  templateUrl: './renderer.component.html',
  styleUrls: ['./renderer.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RendererComponent implements Renderer, SimulationObserver, OnInit, AfterViewInit {

  /*private ap: ApplicationRef, private cd: ChangeDetectorRef, private _differs: KeyValueDiffers, private _itdiffers: IterableDiffers*/
  constructor(protected readonly simulation: SimulationService, private ref: ChangeDetectorRef) {
    console.log('injected service: ' + this.simulation);
  }

  onSimulationChange(): void {
    console.debug('simulation was changed');
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    /* must be after view is initialized, because drawing must occur after initialization*/
    this.simulation.observeBy(this);
  }

}
