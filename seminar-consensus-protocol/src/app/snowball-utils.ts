import {Link} from "./model/link";
import {Node} from "./model/node";
import {ColorState} from "./model/color-state";
import {SnowballSamplingEvent} from "./model/snowball-sampling-event";

/**
 * Returns a sample of the array of given size
 * @param {Link[]} links
 * @param {number} size
 * @returns {Link[]} sample of given array of given size
 */
export function sample<T>(links: T[], size: number): T[] {
  if(size > links.length) {
    throw new Error('Sample size cannot be greater than the set sampled from');
  } else if(size === links.length) {
    return links;
  }

  let shuffled = links.slice(0), i = links.length, temp, index;
  while (i--) {
    index = Math.floor((i + 1) * Math.random());
    temp = shuffled[index];
    shuffled[index] = shuffled[i];
    shuffled[i] = temp;
  }
  return shuffled.slice(0, size);
}

export function query(links: Link[], col: ColorState, samplingNode: Node): {[ColorState.Red]: number, [ColorState.Blue]: number} {
  let stateSampling = {[ColorState.Red]: 0, [ColorState.Blue]: 0};
  for (let link of links) {
    let colorOfSucc: ColorState = response(link.node2);
    if(colorOfSucc === ColorState.None) {
      //todo is different in byzantine protocols
      link.node2['col'] = col;
      colorOfSucc = col;
    }

    stateSampling[colorOfSucc]++;
  }
  return stateSampling;
}

/**
 * This function simulates an node request. Could be considered as unnecessary but keeps the naming of the prototype
 * algorithm.
 * @param {Node} v node from which the response is of
 * @returns {ColorState} the answer to the color state of the given node
 */
export function response(v: Node): ColorState {
  return v['col'];

}

export function createSamplingEvent(samplingNode: Node, link: Link) {
  return new SnowballSamplingEvent(samplingNode, link);
}