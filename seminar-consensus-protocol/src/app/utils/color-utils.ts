import * as Color from "color";
import {ColorState} from "../model/color-state";


/**
 * Returns a color from a set of color shades between two given colors. ShadeCount specifies how fine grade the color
 * set should be. Degree declares which element you receive.
 * @param {number} shadeCount specifies the grade of color set
 * @param {number} degree specifies which element from the color set is given
 * @param {Color} startColor specifies the lower frame of the color set
 * @param {Color} targetColor specifies the upper frame of the color set
 * @returns {Color} returns the specified color from a set of color shades
 */
export function getColorShades(shadeCount: number, degree: number, startColor, targetColor) {
  let color = Object.create(startColor);
  if (shadeCount <= 0) {
    throw new Error('Shades of color must be positive.');
  }

  let r_nuance;
  let g_nuance;
  let b_nuance;

  if (shadeCount === 1) { // shade count must be identity function
    if(!(startColor.hex() === targetColor.hex())) {
      throw new Error('Can only generate one shade if start and target color are the same.');
    }
    r_nuance = 0;
    g_nuance = 0;
    b_nuance = 0;
  } else {
    r_nuance = (targetColor.red() - startColor.red()) / (shadeCount - 1);
    g_nuance = (targetColor.green() - startColor.green()) / (shadeCount - 1);
    b_nuance = (targetColor.blue() - startColor.blue()) / (shadeCount - 1);
  }

  // note shade can be negative
  color = color
    .red(startColor.red() + (degree * r_nuance))
    .green(startColor.green() + (degree * g_nuance))
    .blue(startColor.blue() + (degree * b_nuance));

  return color;
}

export function flip(color:ColorState): ColorState {
  if(color === ColorState.Red) {
    return ColorState.Blue;
  }else if(color === ColorState.Blue) {
    return ColorState.Red;
  } else {
    return ColorState.None;
  }
}

export function getColorMixedShades(shadeCount, degree, color1, color2, mixingRatio = 0.5) {
  let mixing_nuance = mixingRatio / (shadeCount - 1);

  color1 = color1.mix(color2, degree * mixing_nuance);

  return color1;
}