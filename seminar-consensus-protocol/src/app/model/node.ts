import {Protocol} from "./protocol";
import {Link} from "./link";

/**
 * every node has an unique positive identification number
 */
export class Node {
  private leavingLink: Link[] = [];
  private enteringLink: Link[] = []
  constructor(public protocol: Protocol, public readonly nodeID: number) {
    if(nodeID < 0) {
      console.warn('Node id with negative ID');
    }
  }


  public addLink(link: Link) {
    if(link.node1.nodeID === this.nodeID) {
      this.leavingLink.push(link);
    } else if(link.node2.nodeID === this.nodeID) {
      this.enteringLink.push(link);
    } else {
      throw new Error('Illegal state. Added link is not attached to this node (Node ' + this.nodeID + ')');
    }
  }



  public removeLink(link: Link) {
    var index = this.leavingLink.indexOf(link);
    var index2 = this.enteringLink.indexOf(link);
    if (index > -1) {
      this.leavingLink.splice(index, 1);
    }
    if (index > -1) {
      this.enteringLink.splice(index2, 1);
    }
  }

  public getLeavingLinks(): Link[] {
    return this.leavingLink;
  }

  public getEnteringLinks(): Link[] {
    return this.enteringLink;
  }
}