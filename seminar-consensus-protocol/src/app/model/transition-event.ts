import {Link} from "./link";
import {Event} from "./event";

export class TransitionEvent implements Event {
  readonly entryTime: Date;
  readonly link: Link;
  readonly depatureTime: Date;

  constructor(link: Link) {
    this.link = link;
    this.depatureTime = new Date();
    this.entryTime = new Date(this.depatureTime.getTime() + link.delay);
  }


  toString(): string {
    return '('+ this.link.node1 + ', '+ this.link.node2+'): ' + this.entryTime.getTime();
  }


}