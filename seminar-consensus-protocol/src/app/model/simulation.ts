import {Network} from "./network";
import {TransitionEvent} from "./transition-event";
import {Event} from "./event";
import {compareEvent} from "./comparator";
import FastPriorityQueue from "fastpriorityqueue";
import {Node} from "./node";
import {ObservableSimulation} from "./observable-simulation";


//todo check how to forbid dynamic property assign for every attribute except network
//todo check how dynamic attribute adding can be typed. e.g confidence counter in node via snowball protocol
export class Simulation extends ObservableSimulation {

  public eventQueue: FastPriorityQueue<Event> = new FastPriorityQueue((x: Event, y: Event) => compareEvent(x, y) < 0);

  private isRunning: boolean = false;
  private isFinished:boolean = false;

  constructor(public network: Network, public initial: Node, public time: number, public args: {[key: string]: any;}[]) {
    super();
    this.varArgsToSpecificParam(args);
  }

  private varArgsToSpecificParam(args: {[key: string]: any;}[]) {
    if(!args) {
      return;
    }

    /* saves arbitrary variables in simulation class e.g. maxConfidenceCounter = 6; */
    for(let entry of args) {
      let key = Object.keys(entry)[0]; //safe due to type of args
      this[key] = entry[key];
    }
  }

  //todo set checkIfFinished variable instead
  public checkIfFinished(): boolean {
    return this.eventQueue.isEmpty() && this.isRunning;
  }

  public doStep() {
    // initial run
    if(!this.isRunning && !this.checkIfFinished()) {
      if(this.initial == null) {
        throw new Error('No initial node is defined');
      }
      this.isRunning = true;


      //todo set initial event in protocol.initial method. give protocol.run this event instead of null and run code below
      this.initial.protocol.run(this, this.initial, null);
      this.notifyObservers();
      if(this.checkIfFinished()) {
        console.log('simulation ended');
      }
      return;
    }

    if(this.checkIfFinished()) {
      throw new Error('SimulationService is finished');
    }

    let event: Event = this.eventQueue.poll();
    if (event instanceof TransitionEvent) {
      let nextNode: Node = (<TransitionEvent> event).link.node2;

      nextNode.protocol.run(this, nextNode, event);
    } else {
      //todo not implemented yet
    }
    this.notifyObservers();
    if(this.checkIfFinished()) {
      console.log('Simulation ended');
    }
  }


  public running(): boolean {
    return this.isRunning;
  }

}