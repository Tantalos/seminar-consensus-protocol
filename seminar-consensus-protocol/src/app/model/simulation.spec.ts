import {SimulationService} from "./simulation.service";
import {Node} from './node';
import {Link} from "./link";

// describe('tests manipulation of the simulation model', () => {
//   let simulation = new SimulationService();
//   let node0;
//   let node1;
//   let node2;
//   let node3;
//   let node4;
//   let node5;
//
//   let legalLinks;
//   let otherLegalLinks;
//
//   beforeEach(() => {
//     node0 = new Node(null, 0);
//     node1 = new Node(null, 1);
//     node2 = new Node(null, 2);
//     node3 = new Node(null, 3);
//     node4 = new Node(null, 4);
//     node5 = new Node(null, 5);
//
//     let link0 = new Link(0, node1, node3);
//     let link1 = new Link(0, node3, node1);
//     let link2 = new Link(0, node4, node5);
//     let link3 = new Link(0, node1, node2);
//     let link4 = new Link(0, node0, node1);
//
//     legalLinks = [link0, link1, link2, link3, link4];
//     otherLegalLinks = [new Link(0,node4, node2), new Link(0,node4, node0)];
//     //ensures that the two legal link sets above are disjoint
//     expect(!legalLinks.includes.apply(otherLegalLinks));
//
//     simulation.simulation$.network.nodes = [ node0, node1, node2, node3, node4, node5];
//   });
//
//   it('addLink adds all links to the simulation model', () => {
//     simulation.addLinks(legalLinks);
//     expect(simulation.simulation$.network.links).toEqual(legalLinks);
//   });
//
//   it('addLink adds all links to the simulation model without removing old ones', () => {
//     simulation.simulation$.network.links = otherLegalLinks;
//     simulation.addLinks(legalLinks);
//
//     for (let link of legalLinks.concat(otherLegalLinks)) {
//       expect(simulation.simulation$.network.links).toContain(link);
//     }
//   });
//
//   it('updateLink adds all new links to the simulation model and removes the old ones', () => {
//
//     let differentLegalLinks = [new Link(0,node4, node2), new Link(0,node4, node0)];
//     //if conditions fails, test won't make sense
//     expect(legalLinks.includes(differentLegalLinks));
//
//     simulation.simulation$.network.links = differentLegalLinks;
//     simulation.setLinks(legalLinks);
//     expect(simulation.simulation$.network.links).toEqual(legalLinks);
//   });
//
//   it('add link recognizes all links which have as departure node an unknown node', () => {  });
//
//   it('add link recognizes all links which have as arrival node an unknown node', () => {  });
//
//   it('add link recognizes all links which have two unknown node', () => {  });
// });