import {Node} from "./node";
import {Link} from "./link";

export class Network {
  //todo Network should be a class which contains only legal network graphs.
  //todo define rules like bidirectional linking, no reflexive linking etc.

  public nodes: Node[] = [];
  public links: Link[] = [];

  constructor(nodes: Node[], link: Link[]) {
    this.nodes = nodes;
    this.links = link;
  }


}