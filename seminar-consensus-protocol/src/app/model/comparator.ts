import {Event} from "./event";

export function compareEvent(first: Event, second: Event): number {
  return first.entryTime.getTime() - second.entryTime.getTime();
}