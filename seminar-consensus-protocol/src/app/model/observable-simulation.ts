import {SimulationObserver} from "./simulation-observer";

export class ObservableSimulation {
  private observers: SimulationObserver[] = [];

  constructor() {}

  //todo make private and allow only simulation service to access this function. maybe by constructor attribute
  public observeBy(observer: SimulationObserver) {
    if (observer) {
      this.observers.push(observer);
    } else {
      throw new Error('Observer can not be null');
    }
    this.notifyObservers();
  }

  public notifyObservers() {
    for(let o of this.observers) {
      o.onSimulationChange(this);
    }
  }
}