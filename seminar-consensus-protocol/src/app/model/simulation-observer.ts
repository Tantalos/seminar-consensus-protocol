
export interface SimulationObserver {
  onSimulationChange(context: any);
}