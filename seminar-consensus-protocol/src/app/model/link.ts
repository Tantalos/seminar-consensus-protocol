import {Node} from "./node";

export class Link {

  constructor(public delay: number, public node1: Node, public node2: Node) {

  }
}