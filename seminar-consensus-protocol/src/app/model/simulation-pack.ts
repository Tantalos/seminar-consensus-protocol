import {Protocol} from "./protocol";
import {Type} from "@angular/core";


export class SimulationPack {
  constructor(private protocol: Protocol, private renderer: Type<any>) {}

  public getProtocol() {
    return this.protocol;
  }

  public getRenderer() {
    return this.renderer;
  }
}