import {Protocol} from "./protocol";
import {Simulation} from "./simulation";
import {ColorState} from "./color-state";
import {TransitionEvent} from "./transition-event";
import {SnowballSamplingEvent} from "./snowball-sampling-event";
import {sample, query, createSamplingEvent} from "../snowball-utils";
import {Link} from "./link";
import {Node} from "./node";
import {snowballLog} from "../log-config";
import {flip} from "../utils/color-utils";

export class SnowballByzantine implements Protocol {
  public readonly name: string = 'snowball-byzantine';

  initial(current: Node, context: Simulation) {
    //todo check if all parameters are set in simulation

    snowballLog.debug('Initializing Node ' + current.nodeID +
      ' with snowball specific parameter');

    // 2: col := col0, lastcol := col0, cnt := 0
    // 3: d[R] := 0, d[B] := 0

    /* input parameter */
    if (current === context.initial) {
      snowballLog.debug('Initial Node ' + current.nodeID +
        ' starts with color ' + ColorState[context['initialColor']]);
      current['col'] = context['initialColor'];
    } else {
      current['col'] = ColorState.None;
    }

    current['lastcol'] = ColorState.None;
    current['cnt'] = 0;
    let d = {[ColorState.Red]: 0, [ColorState.Blue]: 0};
    current['d'] = d;
    current['accepted'] = false;


    //byzantine value
    current['realColor'] = ColorState.None;
  }


  run(context: Simulation, current: Node, event: TransitionEvent): void {
    if(current['accepted'] === true) {
      throw new Error('Illegal State: Node already accepted its state. Protocol should not be called.');
    }

    //todo make initial event or client request event
    //todo also necessary to determine start color
    if(event === null) {//initial event
      snowballLog.info('Running protocol Snowball on initial Node ' + current.nodeID);
      this.run0(context, current);
    }



    if(event instanceof SnowballSamplingEvent) {
      snowballLog.info('Running protocol Snowball on Node ' + current.nodeID + ' triggered by event ' + event + ' using link ' + event.toString());
      let samplingEvent = <SnowballSamplingEvent> event;
      this.run0(context, current);
    }

    if(current['accepted'] === true) {
      snowballLog.info("Node " + current.nodeID + " has accepted state '" + ColorState[current['col']] + "'");
    }

  }

  //todo for now events will be added directly to eventQueue. Better will be a decorated node which will add it to the queue
  //todo figure out how to handle answers from other nodes, since answers can be a lie and not always fetched from other nodes
  //todo Maybe via promises
  private run0(context: Simulation, current: Node) {

    // since col is always filpped use real color instead.
    if(current['realColor'] === ColorState.None) {
      current['realColor'] = current['col'];
    }
    let col = current['realColor'];
    let lastcol = current['lastcol'];
    let cnt = current['cnt'];
    let d = current['d'];
    let N = current.getLeavingLinks();

    let beta = context['maxConfidenceCounter'];
    let k = context['sampleSize'];
    let alpha = context['alpha'];


    snowballLog.debug('Running Snowball Byzantine on Node ' + current.nodeID +
      ' with [real color=' + ColorState[col] +
      ', last color=' + ColorState[lastcol] +
      ', counter=' + cnt +
      ', confidence counter={' + ColorState[ColorState.Red] + '=' + d[ColorState.Red] +
      ', ' + ColorState[ColorState.Blue] +'=' + d[ColorState.Blue] + '}' +
      ', beta=' + beta +
      ', k=' + k +
      ', alpha=' + alpha + ']'
    );


    /* start protocol */

    // 5: if col = ? then continue
    if(col === ColorState.None) {
      return;
    }

    // 6: K := sample(N\u; k)
    let K = sample<Link>(N, k);

    // 7: P := [query(v; col) for v \in K]
    let P: {[ColorState.Red]: number, [ColorState.Blue]: number} = query(K, col, current);
    for(let l of K) {
      context.eventQueue.add(createSamplingEvent(current, l,));
    }

    // 8: for col' \in {R, B} do
    for(let colDash of [ColorState.Red, ColorState.Blue]) {
      // 9: if P.count(col0) >= \alpha k then
      if(P[colDash] >= alpha * k) {
        // 10: d[col0]++
        d[colDash]++;
        // 11: if d[col0] > d[col] then
        if(d[colDash] > d[col]) {
          // 12: col := col0
          col = colDash;
        }
        // 13: if col0 6= lastcol then
        if(colDash !== lastcol) {
          // 14: lastcol := col0, cnt := 0
          lastcol = colDash;
          cnt = 0;
          // 15: else
        } else {
          // 16: if ++cnt > beta then
          if(++cnt > beta) {
            // accept(col)
            current['accepted'] = true;
          }
        }
      }
    }
    /* end protocol */


    // Not needed if attributes were used directly
    current['lastcol'] = lastcol;
    current['cnt'] = cnt;
    current['d'] = d;
    //save real color to work with it the next round
    current['realColor'] = col;
    //store flipped color to, so nodes sampling this node will get wrong color
    current['col'] = flip(col);


    snowballLog.debug('Snowball Byzantine ended on Node ' + current.nodeID +
      ' with [fake color=' + ColorState[col] +
      ', last color=' + ColorState[lastcol] +
      ', counter=' + cnt +
      ', confidence counter={' + ColorState[ColorState.Red] + '=' + d[ColorState.Red] +
      ', ' + ColorState[ColorState.Blue] +'=' + d[ColorState.Blue] + '}' +
      ', beta=' + beta +
      ', k=' + k +
      ', alpha=' + alpha + ']'
    );
  }



}