import {TransitionEvent} from "./transition-event";
import {Link} from "./link";
import {Node} from "./node";

export class SnowballSamplingEvent extends TransitionEvent {
  /* input variables */
  public readonly u : Node;

  constructor(samplingNode: Node,  link: Link) {
    super(link);
    this.u = samplingNode
  }
}