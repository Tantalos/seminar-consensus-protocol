import {Network} from "./network";
import {Simulation} from "./simulation";
import {Node} from "./node";
import {Link} from "./link";
import {Injectable} from "@angular/core";
import {SimulationObserver} from "./simulation-observer";
import {simLog} from "../log-config";
import {Protocol} from "./protocol";

/**
 * Simulator service is an injectable angular service which works as an simulator factory.
 */
@Injectable({
  providedIn: 'root',
})
export class SimulationService {

  //running simulation with context information
  public simulation$: Simulation;
  public observers: SimulationObserver[] = [];

  public args: {[key: string]: any;}[] = [{maxConfidenceCounter: 6}, {sampleSize: 5}, {alpha: 0.6}];

  //configuring parameter
  public nodes: Node[] = [];
  public links: Link[] = [];

  private initial: Node = null;


  constructor() {
    this.buildObservedSimulation();
    simLog.info('Create Simulation with ' + this.nodes.length + ' Nodes, ' +
    this.links.length + ' Links, Initial node ' + this.initial + ' and args ' +
    this.args );
  }

  public observeBy(observer: SimulationObserver) {
    this.observers.push(observer);
    this.simulation$.observeBy(observer);
  }

  private buildObservedSimulation() {
    let network = this.buildNetwork();
    let simulation = new Simulation(network, this.initial, 0, this.args);

    // initialize nodes according to protocol
    simLog.info('Initialize nodes with start parameters');
    for(let node of network.nodes) {
      node.protocol.initial(node, simulation);
    }

    this.addObservers(simulation);

    this.simulation$ = simulation;
    this.simulation$.notifyObservers();
  }

  private addObservers(simulation: Simulation) {
    for (let o of this.observers) {
      simulation.observeBy(o);
    }
  }

  private buildNetwork(): Network {
    //todo add verification for feasible networks
    return new Network(this.nodes, this.links);
  }

  // updateNetwork(network: Network) {
  //   this.runningGuard();
  //   //todo to be implemented
  //   throw new Error('not yet implemented');
  // }

  public setInitialNode(initial: Node) {
    //todo check if initial node is part of network!

    //TODO NOTE this is overwritten in protocol initial. Have to come up with better solution!!
    //TODO stub, should be part of the protocol
    simLog.info('Initial node was changed to Node ' + initial.nodeID);
    this.initial = initial;
    this.buildObservedSimulation();
  }

  public setNodes(nodes: Node[]): void {
    // remove links, because if every node is removed there are not links which can be leftover
    this.nodes = nodes;
    this.buildObservedSimulation();
  }

  public setLinks(links: Link[]): void {
    this.links = links;
    this.buildObservedSimulation();
  }

  public addNodes(nodes: Node[]): void {
    for(let node of nodes) {
      this.nodes.push(node);
    }
    this.buildObservedSimulation();
  }

  public setProtocolSpecificArgs(args: {[key: string]: any;}[]) {
    this.args = args;
    this.buildObservedSimulation();
  }

  public setProtocol(index: number, protocol: Protocol) {
    let replaceIndex = this.nodes.findIndex((node: Node) => {
      return node.nodeID === index;
    });

    if(replaceIndex === undefined) {
      throw new Error('Node with index ' + index + ' does not exist.');
    }

    this.nodes[replaceIndex].protocol = protocol;

    this.buildObservedSimulation();
  }

      //todo
  // removeNodes(nodes: Node[]) {
  //   this.runningGuard();
  //   // removes every link which contains nodes which should be removed
  //   this.network.links = this.network.links.filter(
  //     (link: Link) => {
  //       return !(nodes.includes(link.node1) || nodes.includes(link.node2));
  //     });
  //
  //   // removes every node which should be removed
  //   this.network.nodes = this.network.nodes.filter((node) => {
  //     return !nodes.includes(node);
  //   });
  // }

  addLinks(links: Link[]) {
    for (let link of links) {
      if (!this.nodes.includes(link.node1) || !this.nodes.includes(link.node2)) {
        throw new Error('Cannot add link with unknown node.');
      }
    }

    this.links = this.links.concat(links);
    this.buildObservedSimulation();
  }

  removeLinks(links: Link[]) {
    this.links = this.links.filter((link: Link) => {return !links.includes(link);});
    this.buildObservedSimulation();
  }

  /**
   * @deprecated
   */
  private runningGuard(): void {
    if ((<Simulation>this.simulation$).running()) {
      throw new Error('SimulationService is already running');
    }
  }
}