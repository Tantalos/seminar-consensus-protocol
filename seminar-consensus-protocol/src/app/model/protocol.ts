import {Simulation} from "./simulation";
import {Node} from "./node";
import {TransitionEvent} from "./transition-event";

export interface Protocol {

  readonly name: string;

  initial(current: Node, context: Simulation);
  run(context: Simulation, current: Node, event: TransitionEvent) : void;
}