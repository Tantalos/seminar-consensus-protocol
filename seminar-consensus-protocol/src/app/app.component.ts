import {Component, OnInit, Type} from '@angular/core';
import {ProtocolServiceService} from "./protocol-service.service";
import {SimulationPack} from "./model/simulation-pack";



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  simulationPack: SimulationPack[] = null;

  constructor(private rendererService: ProtocolServiceService){}

  ngOnInit(): void {
    this.simulationPack = this.rendererService.getSimulationPack();
  }
}
