import {Injectable, Type} from '@angular/core';
import {SnowballRendererComponent} from "./snowball-renderer/snowball-renderer.component";
import {TextRendererComponent} from "./text-renderer/text-renderer.component";
import {SimulationPack} from "./model/simulation-pack";
import {Snowball} from "./model/snowball";
import {Protocol} from "./model/protocol";
import {Simulation} from "./model/simulation";

@Injectable({
  providedIn: 'root'
})
export class ProtocolServiceService {

  constructor() { }


  getSimulationPack(): SimulationPack[] {
    let renderer: Type<any>[] = [SnowballRendererComponent, TextRendererComponent];

    let packs = [
      new SimulationPack(new Snowball(), SnowballRendererComponent),
      new SimulationPack(new Snowball(), TextRendererComponent)
    ];
    return packs;
  }
}
