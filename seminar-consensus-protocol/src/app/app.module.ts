import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {SimulationComponent} from './simulation/simulation.component';
import {SnowballRendererComponent} from './snowball-renderer/snowball-renderer.component';
import {RenderDirective} from './render.directive';
import {TextRendererComponent} from './text-renderer/text-renderer.component';
import {MatInputModule, MatFormFieldModule, MatIconModule, MatButtonModule} from '@angular/material';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FormsModule} from "@angular/forms";
import { RendererComponent } from './renderer/renderer.component';

import {HttpClientModule} from "@angular/common/http";
import { SnowballGraphComponent } from './snowball-graph/snowball-graph.component';
import { SnowballControllerComponent } from './snowball-controller/snowball-controller.component';



@NgModule({
  declarations: [
    AppComponent,
    SimulationComponent,
    SnowballRendererComponent,
    RenderDirective,
    TextRendererComponent,
    RendererComponent,
    SnowballGraphComponent,
    SnowballControllerComponent
  ],
  imports: [
    BrowserModule,

    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatIconModule,

    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule


  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [SnowballRendererComponent, TextRendererComponent]
})
export class AppModule {
}
